import UIKit
import FirebaseAuth

class CreateAccountController: UIViewController, UITextFieldDelegate {
   
    @IBOutlet weak var emailTextFiled: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confirmPasswordTextField.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (passwordTextField.text! != confirmPasswordTextField.text! ){
            showErrorAlert(withMessage: "Passwords don't match!")
        }
    }
    
    func showErrorAlert(withMessage message:String){
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func createButton(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailTextFiled.text!, password: passwordTextField.text!){ (result,error) in
            if let _ = error {
                self.showErrorAlert(withMessage: error?.localizedDescription ?? "Error")
            } else {
                let mainViewController = self.storyboard?.instantiateViewController(withIdentifier: "UserInfoViewController") as! UserInfoViewController
                self.present(mainViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
