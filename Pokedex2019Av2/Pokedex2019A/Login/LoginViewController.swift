import UIKit
import FirebaseAuth
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var pokedexLabel: UILabel!
    @IBOutlet weak var imagenImage: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var pokedexImageView: UIImageView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pokedexImageView.image = UIImage(named: "Ditto")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let db = Firestore.firestore()
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        
        
        let userRef = db.collection("user-info").document(currentUser.uid)
        
        userRef.getDocument { (snapshot, error) in
            self.activityIndicator.stopAnimating()
            if (error != nil) || (snapshot?.get("registerCompleted") == nil){
                self.performSegue(withIdentifier: "toUserInfoView", sender: self)
            } else {
                self.performSegue(withIdentifier: "toMainScreenView", sender: self)
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
    }
    
    @IBAction func loginButton(_ sender: Any) {
//        let mail = "cmamo@gmail.com"
//        let password = "Dieguito23"
//
//        if (mail,password) == (emailTextField.text,passwordTextField.text){
//            performSegue(withIdentifier: "toMainView", sender: self)
//            return
//        }
//
//        print("Credentials Error")
//        showAlertError()
        firebaseAuth(email: emailTextField.text!, password: passwordTextField.text!)
    }
    
    func firebaseAuth (email:String, password:String){
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let _ = error {
                self.showAlertError()
                return
            }
            self.performSegue(withIdentifier: "toUserInfoView", sender: self)
        }
        
    }
    
    @IBAction func createAccountButton(_ sender: Any) {
    }
    
    func showAlertError(){
        let alertView = UIAlertController(title: "Error", message: "Please enter valid Credential", preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title:"OK",style: .default) { [unowned self] (_) in
            self.passwordTextField.text = ""
        }
        
        alertView.addAction(okAlertAction)
        
        present(alertView, animated: true, completion: nil)
    }
    
}
