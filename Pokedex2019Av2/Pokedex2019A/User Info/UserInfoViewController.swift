//
//  UserInfoViewController.swift
//  Pokedex2019A
//
//  Created by Jose Leon on 5/10/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

extension Date {
    static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd MMM yyyy"
        return formatter
    } ()
    
    var formatter: String{
        return Date.formatter.string(from: self)
    }
}

class UserInfoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nickTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    
    let datePickerView = UIDatePicker()
    
    let imagePickerController = UIImagePickerController()
    
    let userId = String(Auth.auth().currentUser!.uid)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        imagePickerController.delegate = self
    }

    @objc func handleDatePicker(_ datePicker:UIDatePicker){
        birthdayTextField.text = datePickerView.date.formatter
    }
    
    override func viewWillAppear(_ animated: Bool) {
        pictureImageView.layer.cornerRadius = pictureImageView.frame.height / 2.0
        pictureImageView.layer.masksToBounds = true
        
        birthdayTextField.inputView = datePickerView
    }
    
    @IBAction func signOutButton(_ sender: Any) {
        try! Auth.auth().signOut()
        let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(loginViewController, animated: true, completion: nil)
    }
    
    @IBAction func addPictureButtonPressed(_ sender: Any) {
        
        selectSource()
    
    }
    
    func saveImage(){
        
        
        let storage = Storage.storage()
        let usersImages = storage.reference().child("users")
        let currentUserImage = usersImages.child("\(userId).jpg")
        
        let userImage = pictureImageView.image
        let data = userImage?.jpegData(compressionQuality: 1)
        
        let updloadTask = currentUserImage.putData(data!, metadata: nil) { (metadata, error) in
            guard let metadata = metadata else {
                return
            }
            let size = metadata.size
            currentUserImage.downloadURL { (url, error) in
                guard let downloadurl = url else {
                    print("error")
                    return
                }
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("error picking image")
            return
        }
        
        pictureImageView.image = image
    }
    
    func selectSource(){
        let alertController = UIAlertController(title: "Select Source", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction (title: "Camera", style: .default) { (_) in
            self.imagePickerController.sourceType = .camera
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let photoAlbumAction = UIAlertAction(title: "Photo Album", style: .default) { (_) in
            self.imagePickerController.sourceType = .photoLibrary
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController,animated: true, completion: nil)
        }
        
        let savedPhotoAlbumAction = UIAlertAction(title: "Saved Photo Album", style: .default) { (_) in
            self.imagePickerController.sourceType = .savedPhotosAlbum
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController,animated: true, completion: nil)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photoAlbumAction)
        alertController.addAction(savedPhotoAlbumAction)
        alertController.addAction(cancel)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        saveImage()
        let db = Firestore.firestore()
        db.collection("user-info").document(userId).setData([
            "name": nameTextField.text ?? "",
            "nick": nickTextField.text ?? "",
            "birthday": Timestamp(date: datePickerView.date),
            "registerCompleted" : true
        ]) { (error) in
            print(error ?? "no error")
        }
    }
    
}
