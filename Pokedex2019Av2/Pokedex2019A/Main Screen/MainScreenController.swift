//
//  MainScreenController.swift
//  Pokedex2019A
//
//  Created by Jose Leon on 5/10/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import Alamofire
import AlamofireObjectMapper
import Kingfisher
import RealmSwift

class MainScreenController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate{

    @IBOutlet weak var pokedexTableView: UITableView!
    var nextUrl = "https://pokeapi.co/api/v2/pokemon"
    var pokemon = [Pokemon]()
    var searchController: UISearchController!
    
    var filteredPokemon = [Pokemon]()
    var isFiltering = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPokemon()
        let realm = try! Realm()
        print(realm.configuration.fileURL)
        realm.objects(PokemonEntity.self).filter("isFavorite = true")
        configureSearchBar()
    }
    
    func getPokemon(){
        Alamofire
            .request(URL(string: nextUrl)!)
            .responseObject{ (response : DataResponse<PokemonResponse>) in
            self.pokemon += response.value?.pokemon ?? [] //se usa mas igual para que el arreglo vaya acumulando  los pokemons que se van decargando
            self.pokedexTableView.reloadData()
            self.nextUrl = response.value?.nextUrl ?? "https://pokeapi.co/api/v2/pokemon"
            }
        }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toPokemonSegue", sender: self)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //numero de filas por sección
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering ? filteredPokemon.count : pokemon.count
    }
    
    //indexPath es la posicion de la celda
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        // Se instancia la celda con el identificador y se le hace un cast a PokemonTableViewCell
        
        
        var currectPokemon :Pokemon!
        
        if isFiltering {
            currectPokemon = filteredPokemon[indexPath.row]
        }else{
            currectPokemon = pokemon[indexPath.row]
        }
        
        
        
        
        cell.pokemonNameLabel.text = pokemon[indexPath.row].name?.capitalized ?? "n/a"
        
        let pokemonId = (pokemon.firstIndex{$0.name == currectPokemon.name} ?? 0) + 1
        
        cell.pokemonImageView.kf.setImage(with: URL(string: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(indexPath.row + 1).png")!)
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == pokemon.count - 1{
            getPokemon()
        }
    }
    
    //Cuando intente mostrar el pokemon numero veinte va a descargar los siguiente 20 pokemones
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPokemonSegue" {
            let destination = segue.destination as! PokemonViewController
            destination.pokemonIndex = pokedexTableView.indexPathForSelectedRow?.row
            
            var selectedPokemon : Pokemon!
            if isFiltering{
                selectedPokemon = filteredPokemon[pokedexTableView.indexPathForSelectedRow?.row ?? 0]
                
            }else{
                selectedPokemon = pokemon[pokedexTableView.indexPathForSelectedRow?.row ?? 0]
           
            }
            
            searchController.dismiss(animated: true){
                self.searchController.searchBar.text = ""
            }
            destination.pokemonUrl = selectedPokemon.url!
            destination.pokemonName = selectedPokemon.name
            
            
            
            
        }
    }
    
    func configureSearchBar(){
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = true
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        
        pokedexTableView.tableHeaderView = searchController.searchBar
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredPokemon = pokemon.filter{($0.name ?? "").lowercased().contains((searchController.searchBar.text ?? "").lowercased())}
        
        isFiltering = searchController.searchBar.text != ""
        pokedexTableView.reloadData()
    }
    
    

}
