//
//  FavPokemonTableViewCell.swift
//  Pokedex2019A
//
//  Created by Johanna Cerezo on 6/18/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit

class FavPokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var favImage: UIImageView!
    
    @IBOutlet weak var favNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
