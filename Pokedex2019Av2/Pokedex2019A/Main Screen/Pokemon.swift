//
//  Pokemon.swift
//  Pokedex2019A
//
//  Created by Jose Leon on 6/4/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import Foundation
import ObjectMapper

class PokemonResponse:Mappable{
    var pokemon:[Pokemon]?
    var nextUrl:String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        pokemon <- map["results"]
        nextUrl <- map["next"]
    }
}

class Pokemon:Mappable {
    var pokemonId:Int?
    var name:String?
    var height:Double?
    var weight:Double?
    var imageUrl:String?
    var types:[PokemonType]?
    var url:String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        pokemonId <- map["id"]
        name <- map["name"]
        height <- map["height"]
        weight <- map["weight"]
        imageUrl <- map["sprites.front_default"]
        types <- map["types"]
        url <- map["url"]
    }
    
    
}

class PokemonType:Mappable {
    var name:String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        name <- map["type.name"]
    }
}
