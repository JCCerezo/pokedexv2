//
//  PokemonEntity.swift
//  Pokedex2019A
//
//  Created by Johanna Cerezo on 6/18/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import Foundation
import RealmSwift

class PokemonEntity:Object{
    
    @objc dynamic var pokemonId:Int = 0
    @objc dynamic var pokemonUrl:String = ""
    @objc dynamic var name:String = ""
    @objc dynamic var height:Double = 0
    @objc dynamic var weight:Double = 0
    @objc dynamic var imageUrl:String = ""
    @objc dynamic var isFavorite = false
    
    convenience init(pokemon:Pokemon) {
        self.init()
        pokemonId = pokemon.pokemonId!
        pokemonUrl = pokemon.url ?? ""
        name = pokemon.name!
        height = pokemon.height ?? 0
        weight = pokemon.weight ?? 0
        imageUrl = pokemon.imageUrl!
    
    }
    
    
    override static func primaryKey() -> String? {
        return "pokemonId"
    }
 
}

