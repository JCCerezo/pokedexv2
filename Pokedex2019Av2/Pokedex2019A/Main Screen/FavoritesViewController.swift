//
//  FavoritesViewController.swift
//  Pokedex2019A
//
//  Created by Johanna Cerezo on 6/18/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import RealmSwift

class FavoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var pokemons = [PokemonEntity]()

    @IBOutlet weak var favTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPokemon()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getPokemon()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //numero de filas por sección
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "favPokemonCell") as! FavPokemonTableViewCell
        cell.favNameLabel.text = pokemons[indexPath.row].name.capitalized ?? "n/a"
        cell.favImage.kf.setImage(with: URL(string:pokemons[indexPath.row].imageUrl)!)
        return cell
    }
    
    func getPokemon(){
        
        // Se instancia la celda con el identificador y se le hace un cast a PokemonTableViewCell
        let realm = try! Realm()
        var pokemonsResult = realm.objects(PokemonEntity.self).filter("isFavorite = true")
        pokemons.removeAll()
        for pokemon in pokemonsResult{
            
            pokemons.append(pokemon)
        }
        self.favTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toFavPokemonSegue", sender: self)
    }
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "toFavPokemonSegue"{
                let destination = segue.destination as! PokemonViewController
                destination.inFavorite = true
                let selectedPokemon = pokemons[favTable.indexPathForSelectedRow?.row ?? 0]
                destination.pokemonUrl = selectedPokemon.pokemonUrl
                destination.pokemonName = selectedPokemon.name
           
        }
        
        
    }
        
   
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPokemonSegue" {
            let destination = segue.destination as! PokemonViewController
            destination.pokemonIndex = pokedexTableView.indexPathForSelectedRow?.row
            
            let selectedPokemon = pokemon[pokedexTableView.indexPathForSelectedRow?.row ?? 0]
            destination.pokemonUrl = selectedPokemon.url!
            destination.pokemonName = selectedPokemon.name
            
        }*/
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


