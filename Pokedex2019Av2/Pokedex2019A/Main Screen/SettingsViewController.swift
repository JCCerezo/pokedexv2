//
//  SettingsViewController.swift
//  Pokedex2019A
//
//  Created by Johanna Cerezo on 6/14/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Firebase

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func signOutButtonPressed(_ sender: Any) {
        
        do {
            try Auth.auth().signOut()
            dismiss(animated: true, completion: nil)
        }catch{
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
