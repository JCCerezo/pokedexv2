//
//  PokemonViewController.swift
//  Pokedex2019A
//
//  Created by Jose Leon on 6/4/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher
import RealmSwift

class PokemonViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var typeTable: UITableView!
    
    var pokemonIndex:Int?
    var pokemonName: String?
    var pokemonUrl = "https://pokeapi.co/api/v2/pokemon/"
    var types = [PokemonType]()
    var pokemonId: Int?
    var pokemon:Pokemon? //en este atributo se van a guardar los datos que se descargan del pokeapi
    var inFavorite = false

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = pokemonName?.capitalized ?? "Pokemon"
         downloadPokemonInfo()
        
        if(inFavorite){
            let button = UIBarButtonItem(title: "Remove", style: .done, target: self, action: #selector(removeTapped))
            self.navigationItem.rightBarButtonItem = button
            
            
        }else{
            let button = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(favTapped))
            self.navigationItem.rightBarButtonItem = button
            
        }
        
        
        
    }
    
    @objc func favTapped(){
        let userDefaults = UserDefaults.standard
        //forma de guardar datos en la memoriria del telefono reservada para la aplicacion no mayur a 2 MB
        //Solo primitivios o arreglos de primitivos
        

        var favouritePokemon = userDefaults.array(forKey: "favPokemon") ?? []
        favouritePokemon.append(pokemonId)
        userDefaults.set(favouritePokemon, forKey: "favPokemon")
        let realm = try! Realm()
        try! realm.write {
            let pokemonEntity = PokemonEntity(pokemon: pokemon!)
            pokemonEntity.isFavorite = true
            pokemonEntity.pokemonUrl = pokemonUrl
            realm.add(pokemonEntity, update: true)
        }
        
    }
    
    @objc func removeTapped(){
        let realm = try! Realm()
        try! realm.write {
            let pokemonEntity = PokemonEntity(pokemon: pokemon!)
            pokemonEntity.isFavorite = false
            realm.add(pokemonEntity, update: true)
        }
    }
    
    func downloadPokemonInfo (){
        Alamofire.request(pokemonUrl).responseObject { (response: DataResponse<Pokemon>) in
            self.pokemon = response.value
            self.weightLabel.text = "\(response.value?.weight ?? 0)"
            self.heightLabel.text = "\(response.value?.height ?? 0)"
            self.types = response.value?.types ?? []
            self.typeTable.reloadData()
            self.pokemonId = response.value?.pokemonId
            self.pokemonImageView.kf.setImage(with: URL(string: response.value?.imageUrl ?? "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/0.png"))
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = types[indexPath.row].name?.capitalized ?? "n/a"
        return cell
    }
    
 
}
